import java.util.ArrayList;
import java.util.Scanner;

public class Carrera {
	
	public Pista pista;
	public Nave nave;
	public Locutor locutor;
	
	private ArrayList<Reparador> reparadores = new ArrayList<Reparador>();
	private ArrayList<Interfaz_Nave> naves = new ArrayList<Interfaz_Nave>();
	private ArrayList<Interfaz_Nave> ganadores = new ArrayList<Interfaz_Nave>();
	private int cantidad_naves;
	private Scanner sc = new Scanner(System.in);
	private int random;
	private String nombre;
	private String combustible_nave;
	private int duracion_carrera;
	
	Carrera()
	{
		this.locutor = new Locutor();
		duracion_carrera=0;
		locutor.bienvenida();
		System.out.println("Ingrese la cantidad de naves: ");
		cantidad_naves=sc.nextInt();
		this.pista = new Pista(cantidad_naves);
		creacion_de_naves();
		correr_juego();
	}
	
	public void creacion_de_naves()
	{
		sc.nextLine();
		int contador = 0;
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		while (contador < cantidad_naves)
		{
			System.out.println("-Ingrese un nombre para la nave "+ (contador+1) +":");
			nombre=sc.nextLine();
			
			System.out.println("-¿Qué combustible usará? Ingrese '1' para Diesel, '2' para BioDiesel y '3' para Combustible Vegetal.");
			combustible_nave = sc.nextLine();
			
			nave = new Nave();
			if (combustible_nave.equals("1"))
			{
				crear_naveA(nombre);
				naves.add(nave);
			}
			if (combustible_nave.equals("2"))
			{
				crear_naveB(nombre);
				naves.add(nave);
			}
			if (combustible_nave.equals("3"))
			{
				crear_naveC(nombre);
				naves.add(nave);
			}
			contador=contador+1;
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		}	
	}

	public int getCantidad_naves() {
		return cantidad_naves;
	}

	public ArrayList<Interfaz_Nave> getGanadores() {
		return ganadores;
	}

	public void crear_naveA(String nombre) {
		
		nave.setNombre_combustible("D");
		nave.setNombre(nombre);
		nave.getTurbina().setVida(120);
		nave.getTurbina().setVida_antes(120);
		
		nave.getEstanque().setVida(100);
		nave.getEstanque().setVida_antes(100);
		
		nave.getAlas().setVida(80);
		nave.getAlas().setVida_antes(80);
		
		nave.getOdometro().setVida(100);
		nave.getOdometro().setVida_antes(100);
		
		nave.getMotor().setVida(150);
		nave.getMotor().setVida_antes(150);
		
		nave.getTurbina().setVelocidad(0);
		nave.getTurbina().setAceleracion(20);
		nave.getOdometro().setDistancia(0);
	}
	public void crear_naveB(String nombre) {
	
		nave.setNombre_combustible("BD");
		nave.setNombre(nombre);
		nave.getTurbina().setVida(110);
		nave.getTurbina().setVida_antes(110);
		
		nave.getEstanque().setVida(110);
		nave.getEstanque().setVida_antes(110);
		
		nave.getAlas().setVida(90);
		nave.getAlas().setVida_antes(90);
		
		nave.getOdometro().setVida(120);
		nave.getOdometro().setVida_antes(120);
		
		nave.getMotor().setVida(140);
		nave.getMotor().setVida_antes(140);
		
		nave.getTurbina().setVelocidad(0);
		nave.getTurbina().setAceleracion(20);
		nave.getOdometro().setDistancia(0);
	}

	public void crear_naveC(String nombre) {
	
		nave.setNombre_combustible("CN");
		nave.setNombre(nombre);
		nave.getTurbina().setVida(130);
		nave.getTurbina().setVida_antes(130);
		
		nave.getEstanque().setVida(90);
		nave.getEstanque().setVida_antes(90);
		
		nave.getAlas().setVida(110);
		nave.getAlas().setVida_antes(110);
		
		nave.getOdometro().setVida(110);
		nave.getOdometro().setVida_antes(110);
		
		nave.getMotor().setVida(160);
		nave.getMotor().setVida_antes(160);
		
		nave.getTurbina().setVelocidad(0);
		nave.getTurbina().setAceleracion(20);
		nave.getOdometro().setDistancia(0);
	}
	
	public void correr_ganadores()
	{
		System.out.println("");
		System.out.println("---------------------tabla ganadores--------------------");
		for (int i=0;i<cantidad_naves;i++)
		{
			System.out.println("LUGAR "+(i+1)+" la nave llamada: "+ganadores.get(i).getNombre());
		}
		System.out.println("--------------------------------------------------------");
		System.out.println("");
	}
	
	public double aceleracion(String combustible,double aceleracion)
	{
		if (combustible=="D")
		{
			aceleracion=aceleracion*1.02;
		}
		if (combustible=="BD")
		{
			aceleracion=aceleracion*1.003;
		}
		if (combustible=="CN")
		{
			aceleracion=aceleracion*1.04;
		}
		return aceleracion;
	}
	
	public double velocidad(double velocidad,double aceleracion)
	{
		velocidad = velocidad+aceleracion;
		return velocidad;
	}
	
	public double velocidad_mas_desgaste(double velocidad, double vida_antes, double vida)
	{
		vida=(vida/vida_antes);
		velocidad=velocidad-(5*vida);
		return velocidad;
	}
	
	public double distancia_final(double distancia, double velocidad)
	{
		distancia = distancia + velocidad;
		return distancia;
	}
	
	public void esperar_segundos(int segundos) {
		try 
		{
			Thread.sleep(segundos);
		} 
		catch (InterruptedException ex) 
		{
			Thread.currentThread().interrupt();
		}
	}
	
	public double degradacion(double velocidad,double vida)
	{
		if (velocidad<50)
		{
			random=(int) (Math.random()*10)+1;
			vida=vida-random;
			if (vida<=0)
			{
				vida=5;
			}
		}
		if ((velocidad>=50)&&(velocidad<=100))
		{
			random=(int) (Math.random()*25)+10;
			vida=vida-random;
			if (vida<=0)
			{
				vida=5;
			}
		}
		if ((velocidad>100)&&(velocidad<=150))
		{
			random=(int) (Math.random()*50)+25;
			vida=vida-random;
			if (vida<=0)
			{
				vida=5;
			}
		}
		return vida;
	}
	
	public void Cambio_en_el_tiempo(int numero)
	{
		//calcula la aceleracion a partir del conbustible
		naves.get(numero).getTurbina().setAceleracion(aceleracion(naves.get(numero).getNombre_combustible(),naves.get(numero).getTurbina().getAceleracion()));
		
		//calcula la velocidad mas la aceleracion
		naves.get(numero).getTurbina().setVelocidad(velocidad(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getTurbina().getAceleracion()));
		
		//calculamos el desgaste de cada parte
		naves.get(numero).getAlas().setVida(degradacion(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getAlas().getVida()));
		naves.get(numero).getTurbina().setVida(degradacion(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getTurbina().getVida()));
		naves.get(numero).getMotor().setVida(degradacion(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getMotor().getVida()));
		naves.get(numero).getOdometro().setVida(degradacion(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getOdometro().getVida()));
		naves.get(numero).getEstanque().setVida(degradacion(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getEstanque().getVida()));
		
		//calculamos la velocidad final, mas el desgaste de la nave
		naves.get(numero).getTurbina().setVelocidad(velocidad_mas_desgaste(naves.get(numero).getTurbina().getVelocidad(),naves.get(numero).getAlas().getVida_antes(),naves.get(numero).getAlas().getVida()));
		
		//distancia recorrida
		naves.get(numero).getOdometro().setDistancia(distancia_final(naves.get(numero).getOdometro().getDistancia(),naves.get(numero).getTurbina().getVelocidad()));
	}
	
	public void Reparando_naves(int numero)
	{
		naves.get(numero).getTurbina().setVelocidad(0);
		naves.get(numero).getTurbina().setAceleracion(20);
		naves.get(numero).getAlas().setVida(pista.getReparadores().get(numero).reparacion(naves.get(numero).getAlas().getVida(), naves.get(numero).getAlas().getVida_antes()));
		naves.get(numero).getTurbina().setVida(pista.getReparadores().get(numero).reparacion(naves.get(numero).getTurbina().getVida(), naves.get(numero).getTurbina().getVida_antes()));
		naves.get(numero).getMotor().setVida(pista.getReparadores().get(numero).reparacion(naves.get(numero).getMotor().getVida(), naves.get(numero).getMotor().getVida_antes()));
		naves.get(numero).getOdometro().setVida(pista.getReparadores().get(numero).reparacion(naves.get(numero).getOdometro().getVida(), naves.get(numero).getOdometro().getVida_antes()));
		naves.get(numero).getEstanque().setVida(pista.getReparadores().get(numero).reparacion(naves.get(numero).getEstanque().getVida(), naves.get(numero).getEstanque().getVida_antes()));
		
		if (naves.get(numero).getAlas().getVida()>naves.get(numero).getAlas().getVida_antes())		
		{
			naves.get(numero).getAlas().setVida(naves.get(numero).getAlas().getVida_antes());
		}
		if (naves.get(numero).getTurbina().getVida()>naves.get(numero).getTurbina().getVida_antes())
		{
			naves.get(numero).getTurbina().setVida(naves.get(numero).getTurbina().getVida_antes());
		}
		if (naves.get(numero).getEstanque().getVida()>naves.get(numero).getEstanque().getVida_antes())
		{
			naves.get(numero).getEstanque().setVida(naves.get(numero).getEstanque().getVida_antes());
		}
		if (naves.get(numero).getMotor().getVida()>naves.get(numero).getMotor().getVida_antes())
		{
			naves.get(numero).getMotor().setVida(naves.get(numero).getMotor().getVida_antes());
		}
		if (naves.get(numero).getOdometro().getVida()>naves.get(numero).getOdometro().getVida_antes())
		{
			naves.get(numero).getOdometro().setVida(naves.get(numero).getOdometro().getVida_antes());
		}	
	}
	
	public void menu_naves()
	{
		System.out.println("___________________________________________________________________________");
		System.out.println("");
		System.out.println("Horas de carrera : "+ duracion_carrera);
		
		for (int i=0;i<cantidad_naves;i++) {
			System.out.println("");
            System.out.println("---Nave: "+naves.get(i).getNombre()+"---");
            System.out.println("-Velocidad: "+naves.get(i).getTurbina().getVelocidad()+ " km/h");
            System.out.println("-Distancia: "+naves.get(i).getOdometro().getDistancia()+ " km");
            System.out.println("Estado de la nave:");
            System.out.println("|Alas: "+naves.get(i).getAlas().getVida()+"");
            System.out.println("|Turbina: "+naves.get(i).getTurbina().getVida());
            System.out.println("|Odometro: "+naves.get(i).getOdometro().getVida());
            System.out.println("|Motor: "+naves.get(i).getAlas().getVida());
            System.out.println("|Estanque: "+naves.get(i).getEstanque().getVida());
            
		}
		System.out.println("___________________________________________________________________________");
		esperar_segundos(2000);
		duracion_carrera=duracion_carrera+1;
	}
	
	public void correr_juego()
	{
		
		int contador_ganadores=0;
		int win=0;
		Nave primera= new Nave();
		Nave primera_anterior=new Nave();
		locutor.en_sus_marcas();
		menu_naves();
		while(contador_ganadores < cantidad_naves)
		{
			for (int i=0;i<cantidad_naves;i++)
			{
				if (naves.get(i).getOdometro().getDistancia()<pista.getDistancia_pista())
				{
					if (naves.get(i).getAlas().getVida()>(naves.get(i).getAlas().getVida_antes()*0.2)&&(naves.get(i).getTurbina().getVida()>(naves.get(i).getTurbina().getVida_antes()*0.2))&&(naves.get(i).getOdometro().getVida()>(naves.get(i).getOdometro().getVida_antes()*0.2))&&(naves.get(i).getMotor().getVida()>(naves.get(i).getMotor().getVida_antes()*0.2))&&(naves.get(i).getEstanque().getVida()>(naves.get(i).getEstanque().getVida_antes()*0.2)))
					{
						Cambio_en_el_tiempo(i);
						if (pista.getDistancia_pista()<=naves.get(i).getOdometro().getDistancia())
						{
							//registra y guarda los ganadores
							naves.get(i).getOdometro().setDistancia(pista.getDistancia_pista());
							naves.get(i).getTurbina().setVelocidad(0);
							ganadores.add(naves.get(i));
							
						    locutor.llega_meta(naves.get(i).getNombre(), ganadores.size() );

							contador_ganadores=contador_ganadores+1;
							if (contador_ganadores==cantidad_naves)
							{
								
								win=1;
								menu_naves();
								System.out.println("");
								System.out.println("DETALLES DE LAS NAVES GANADORAS");
								correr_ganadores();
							}
						}
		
					}
					else
					{
						Reparando_naves(i);
					}
						
				}
				if((primera.getOdometro().getDistancia()<naves.get(i).getOdometro().getDistancia())) {
					primera=(Nave) naves.get(i);
				}
			
			}
			if(duracion_carrera==1) {
				locutor.inicio(primera.getNombre());
			}
			if((primera.getOdometro().getDistancia()>primera_anterior.getOdometro().getDistancia()&&(primera.getTurbina().getVelocidad()!=0))&& duracion_carrera!=1) {
				locutor.delantera(primera_anterior.getNombre() ,primera.getNombre());
				
				
			}
			if(win==0) {
				menu_naves();		
			}
			primera_anterior=primera;
		}
	}
}
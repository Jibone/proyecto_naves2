import java.util.ArrayList;

public class Pista 
{
	public Reparador reparador;
	
	private ArrayList<Reparador> reparadores = new ArrayList<Reparador>();
	private double distancia_pista;
	private int cantidad_naves;
	
	Pista (int cantidad_naves)
	{
		this.cantidad_naves=cantidad_naves;
		this.distancia_pista=1000;
		for (int i=0;i<cantidad_naves;i++)
		{
			reparador=new Reparador();
			reparadores.add(reparador);
		}
	}
	
	public double getDistancia_pista() {
		return distancia_pista;
	}

	public ArrayList<Reparador> getReparadores() {
		return reparadores;
	}
}

public class Motor {
	private double vida;
	private double vida_antes;
	
	public void setVida_antes(double vida_antes)
	{
		this.vida_antes=vida_antes;
	}
	
	public double getVida_antes()
	{
		return vida_antes;
	}

	public double getVida() {
		return vida;
	}

	public void setVida(double vida) {
		this.vida = vida;
	}
}


public class Nave implements Interfaz_Nave{

	private Turbina turbina;
	private Estanque estanque;
	private Alas alas;
	private Odometro odometro;
	private Motor motor;
	private String nombre_combustible;
	private String nombre;

	Nave()
	{
		this.turbina = new Turbina();
		this.estanque = new Estanque();
		this.alas = new Alas();
		this.odometro = new Odometro();
		this.motor = new Motor();
	}
	
	public String getNombre_combustible() {
		return nombre_combustible;
	}
	
	public void setNombre_combustible(String nombre_combustible) {
		this.nombre_combustible = nombre_combustible;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Turbina getTurbina() {
		return turbina;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public Alas getAlas() {
		return alas;
	}

	public Odometro getOdometro() {
		return odometro;
	}

	public Motor getMotor() {
		return motor;
	}

}
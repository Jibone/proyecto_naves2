
public class Turbina {
	private double vida;
	private double aceleracion;
	private double velocidad;
	private double vida_antes;
	
	public void setVida_antes(double vida_antes)
	{
		this.vida_antes=vida_antes;
	}
	
	public double getVida_antes()
	{
		return vida_antes;
	}

	public double getVelocidad() {
		return velocidad;
	}
	
	public double cambio_velocidad()
	{
		return velocidad;
	}

	public void setVelocidad(double velocidad) {
		this.velocidad = velocidad;
	}

	public double getAceleracion() {
		return aceleracion;
	}

	public void setAceleracion(double aceleracion) {
		this.aceleracion = aceleracion;
	}

	public double getVida() {
		return vida;
	}

	public void setVida(double vida) {
		this.vida = vida;
	}
}

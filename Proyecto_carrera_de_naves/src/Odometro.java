
public class Odometro {
	private double vida;
	private double distancia;
	private double vida_antes;
	
	public void setVida_antes(double vida_antes)
	{
		this.vida_antes=vida_antes;
	}
	
	public double getVida_antes()
	{
		return vida_antes;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public double getVida() {
		return vida;
	}

	public void setVida(double vida) {
		this.vida = vida;
	}
}

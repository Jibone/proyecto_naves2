
public class Locutor {
	private int random;
	
	Locutor(){
			
	}
	void bienvenida(){
		System.out.println("¡Bienvenidos a la Super Carrera F-Zero!");
		System.out.println("");
		
	}
	void en_sus_marcas() {
		System.out.println("La carrera comienza en...");
		esperar_segundos(1000);
		System.out.println("3");
		esperar_segundos(1000);
		System.out.println("2");
		esperar_segundos(1000);
		System.out.println("1");
		esperar_segundos(1000);
		System.out.println("GO!");
	}
	void inicio(String primero) {
		System.out.println("");
		random=(int) (Math.random()*3)+1;
		switch(random) {
			case 1:
				System.out.println("¡Y "+primero+" TOMA LA DELANTERA!");
			break;
			
			case 2:
				System.out.println("!"+primero+" SIEMPRE DANDO EL PRIMER PASO!");
			break;
			
			case 3:
				System.out.println("!"+primero+" PARTE DANDO CLASES DE CÓMO SE HACE!");
			break;
		}
	}
	
	void delantera(String primero_viejo , String primero_nuevo ){
		if(primero_viejo!=primero_nuevo) {
			System.out.println("");
			random =(int) (Math.random()*3)+1;
			switch(random) {
				case 1:
					System.out.println("<<<Y viene "+primero_nuevo+" quitándole la delantera a "+ primero_viejo+">>>");
				break;
				
				case 2:
					System.out.println("<<<"+primero_viejo+" se ha confiado mucho y "+primero_nuevo+" le quita la delantera>>>");
				break;
				
				case 3:
					System.out.println("<<<Y "+primero_nuevo+" se hace con el primer puesto por sobre "+primero_viejo+">>>");
				break;
			}
		
		}
	}
	void llega_meta(String nave_llega, int posicion){
		System.out.println("");
		System.out.println("********************************");
		System.out.println("--"+ nave_llega +" llega en la posición N°"+ posicion +" --");
		System.out.println("********************************");
	}
	
	public void esperar_segundos(int segundos) {
		try 
		{
			Thread.sleep(segundos);
		} 
		catch (InterruptedException ex) 
		{
			Thread.currentThread().interrupt();
		}
	}
}

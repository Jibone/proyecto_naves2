
public interface Interfaz_Nave {
	public String getNombre_combustible();
	public void setNombre_combustible(String nombre_combustible);
	public String getNombre();
	public void setNombre(String nombre);

	public Turbina getTurbina();

	public Estanque getEstanque();

	public Alas getAlas();

	public Odometro getOdometro();

	public Motor getMotor();

}

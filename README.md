# Proyecto_Naves2
1) El proyecto correspondería a un programa bajo el lenguaje de programacion java, en el cual será necesario el compilar todos los archivos (.java) junto
a un compilador en torno a clases, como seria el caso de eclipse o intellij IDEA.
2) El proyecto añadirá una imagen (Diagramanaves2.png) la que contendra un diagrama para la mejor comprension del codigo.
3) El programa genera una simulación de una carrera con una N cantidad de naves, cada una compuesta por sus piezas correspondientes (motor, alas, turbinas,
odómetro, estanque), existen 3 prototipos de naves (Nave A, Nave B, y Nave C) en las cuales existen diferencias en la durabilidad de las piezas y el tipo de 
combustible que utiliza (Diesel, BioDiesel, Vegetal),  esta durabilidad se puede ver reflejada mientras transcurre la carrera, dado que por
la velocidad a la que se ven expuestas las naves sus piezas se van a ir desgastando.
4) Es por esto que existen una N cantidad de reparadores los cuales cuando una pieza de alguna nave se encuentra demasiado desgastada, estos se encargan de reparar 
un porcentaje de cada parte de la nave.
5) La carrera transcurre sobre una pista con un largo definido, sobre la cual las naves van avanzando.
6) Existe un locutor, el cual va a ir narrando como transcurre la carrera.
7) A medida que se desarrolla la carrera uno puede ir observando el estado actual de las piezas que componen cada nave, la velocidad actual y la distancia recorrida
hasta este momento.
8) Al momento de necesitar ser reparadas su velocidad baja a 0 km/h mientras dura la reparacion.
9) Finalmente, la nave que consiga recorrer la distancia que representa la pista será la ganadora, de igual manera se presentará una tabla con las posiciones en las 
que terminaron cada nave al final del programa.
